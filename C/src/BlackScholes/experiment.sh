#! /bin/bash

# Run this on the main node, first argument must be another node to which we can
# ssh for launching a minion node.
#
# This script assumes that the assembly algorithm will look first for a remote
# node to map, and if not found, map a local vgpu.

log="experiment.log"

RUNTIME_DIR=$HOME/code/kidron-rce/backend
MAIN_DNS=$HOSTNAME # used only with run_with_restart

iters_per_batch=20 # how many assm joins (app runs) before batch changes
num_sizes=15 # how many times the code changes the batch size

nopts=1024
niter=16384
BSARGS="-device=0 -nopts=$nopts -niter=$niter"

assm_cpumask="0,2,4,6,8"
bs_cpumask="10"

# Set this so the output is more amenable for parsing the timing numbers.
timing=true

if [[ -z $1 ]]
then
	echo "Specify remote node DNS"
	exit 1
fi

set -e
set -u

function run_single {
	for ((iter=0; iter < iters_per_batch; iter++))
	do
		numactl --physcpubind=$bs_cpumask \
			/usr/bin/time -f '%e' \
				../../bin/linux/release/BlackScholes $BSARGS \
					2>&1 | tee -a $log
		sleep 1
	done
}

function run {
	touch $log
	batchsize=1
	for ((batch=0; batch < num_sizes; batch++))
	do
		for ((iter=0; iter < iters_per_batch; iter++))
		do
			if [ timing ]; then
				echo "batch $batchsize " | tee -a $log
			else
				echo -n "batch $batchsize " | tee -a $log
				BSARGS += " -nonewline"
			fi
			numactl --physcpubind=$bs_cpumask \
				/usr/bin/time -f '%e' \
					../../bin/linux/release/BlackScholes $BSARGS \
						2>&1 | tee -a $log
			sleep 1
		done
		batchsize=$((batchsize * 2))
	done
}

function do_baselines {
	# We assume we're executing on the main node.
	rm -f $log
	( cd $RUNTIME_DIR; numactl --physcpubind=$assm_cpumask ./runtime main > runtime-main.log 2>&1 & )
	sleep 2 # give the runtime some space to initialize
	
	unset LD_PRELOAD
	
	echo "Running norm"
	run_single
	mv -v $log bs.norm.txt
	
	export LD_PRELOAD=/nics/b/home/merritt/code/kidron-rce/interposer/libci.so
	
	echo "Running local vgpu"
	run_single
	mv -v $log bs.local.txt

	sleep 1
	killall -s SIGINT runtime

	# DO NOT FORGET TO DO THIS ELSE THE RUNTIME WILL PRELOAD THE DAMN LIBRARY
	unset LD_PRELOAD
}

function do_batch_variation {
	# We assume we're executing on the main node.
	rm -f $log
	( cd $RUNTIME_DIR; numactl --physcpubind=$assm_cpumask ./runtime main > runtime-main.log 2>&1 & )
	sleep 2 # give the runtime some space to initialize

	export LD_PRELOAD=/nics/b/home/merritt/code/kidron-rce/interposer/libci.so
	
	echo "Starting runtime on remote node $1"
	node=$1
	ssh $node "cd $HOME/code/kidron-rce/backend; ./run_minion.sh $HOSTNAME"
	sleep 2 # give the other node some time to join the runtime
	run
	mv -v $log bs.remote.txt
	ssh $node killall -s SIGINT runtime
	
	sleep 1
	
	killall -s SIGINT runtime

	unset LD_PRELOAD
}

#do_baselines
do_batch_variation $1

echo Done
