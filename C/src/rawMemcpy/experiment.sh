#! /usr/bin/env bash

# Run rawMemcpy with a fixed total size transferred per execution, varying how
# many RPCs are used to move it.

DATASIZE=1024
TRIALS=20 # iterations per execution configuration
CPUMASK="11"

########################################
# No configuration below here.

NUM_RPCS_START=2
NUM_RPCS_END=$DATASIZE

set -e
set -u

logDir="log"

function bare {
	rm -rf $logDir
	mkdir $logDir
	for ((rpcs=NUM_RPCS_START; rpcs <= NUM_RPCS_END; rpcs=rpcs * 2))
	do
		iterParam=$((rpcs / 2))
		sizeParam=$((DATASIZE / rpcs))
		log=${sizeParam}.${iterParam}
		for ((trial = 0; trial < TRIALS; trial++))
		do
			numactl --physcpubind=$CPUMASK \
				microtime \
					./rawMemcpy $sizeParam $iterParam -nonewline \
						2>&1 | tee -a $logDir/$log
			sleep 1
		done
	done
}

function assembly {
	logDir="log.assm"
	rm -rf $logDir
	mkdir $logDir
	export LD_PRELOAD=/nics/b/home/merritt/code/kidron-rce/interposer/libci.so
	bare
	unset LD_PRELOAD
}

if [ $# -eq 0 ]
then
	echo "Specify function name"
	exit -1
fi

$1
