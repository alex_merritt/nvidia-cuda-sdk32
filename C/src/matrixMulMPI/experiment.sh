#! /bin/bash

# Run this on the main node, first argument must be another node to which we can
# ssh for launching a minion node.
#
# This script assumes that the assembly algorithm will look first for a remote
# node to map, and if not found, map a local vgpu.

log="experiment.log"

RUNTIME_DIR=$HOME/code/kidron-rce/backend
MAIN_DNS=$HOSTNAME # used only with run_with_restart

runs=20

mult_start=10
mult_end=100
mult_incr=10

if [[ -z $1 ]]
then
	echo "Specify remote node DNS"
	exit 1
fi

set -e
set -u

function run_with_restart {
	for ((mult=$mult_start; mult <= $mult_end; mult += $mult_incr))
	do
		for ((iter=0; iter < runs; iter++))
		do
			# Only start/stop runtime if we're on the minion node to capture the
			# initial overhead from PTX compilation in the driver that contributes
			# to the "faster" execution on the remote path. Comment this and the
			# killall/usleep out if we are not restarting the runtime with each run.
			( cd $RUNTIME_DIR; numactl --physcpubind=0 ./runtime minion $MAIN_DNS > expmt-run.log 2>&1 & )
			usleep 500000
	
			# Prefix numactl with 'assm' command (translates to LD_PRELOAD of the
			# interposing library) to run mm in assemblies.
			assm numactl --physcpubind=3 \
				/usr/bin/time -f '%e' \
					../../bin/linux/release/matrixMul -device=0 -sizemult=$mult -niter=1 \
						2>&1 | tee -a $log
	
			usleep 500000
			killall -s SIGINT runtime
			usleep 500000
		done
	done
}

function run {
	touch $log
	for ((mult=$mult_start; mult <= $mult_end; mult += $mult_incr))
	do
		for ((iter=0; iter < runs; iter++))
		do
			numactl --physcpubind=3 \
				/usr/bin/time -f '%e' \
					../../bin/linux/release/matrixMul -device=0 -sizemult=$mult \
						2>&1 | tee -a $log

			sleep 1
		done
	done
}

# We assume we're executing on the main node.

rm -f $log

( cd $RUNTIME_DIR; numactl --cpunodebind=0 ./runtime main > runtime-main.log 2>&1 & )
sleep 2 # give the runtime some space to initialize

#unset LD_PRELOAD

#echo "Running norm"
#run
#mv -v $log mm.norm.txt

export LD_PRELOAD=/nics/b/home/merritt/code/kidron-rce/interposer/libci.so

echo "Running local vgpu"
run
mv -v $log mm.local.txt

echo "Starting runtime on remote node $1"
node=$1
ssh $node "cd $HOME/code/kidron-rce/backend; ./run_minion.sh $HOSTNAME"
sleep 2 # give the other node some time to join the runtime
run
mv -v $log mm.remote.txt
ssh $node killall -s SIGINT runtime

sleep 1

killall -s SIGINT runtime

echo Done
