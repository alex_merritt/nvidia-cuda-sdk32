/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/* Matrix multiplication: C = A * B.
 * Host code.
 *
 * This sample implements matrix multiplication as described in Chapter 3
 * of the programming guide.
 * It has been written for clarity of exposition to illustrate various CUDA
 * programming principles, not with the goal of providing the most
 * performant generic kernel for matrix multiplication.
 *
 * CUBLAS provides high-performance matrix multiplication.
 * See also:
 * V. Volkov and J. Demmel, "Benchmarking GPUs to tune dense linear algebra,"
 * in Proc. 2008 ACM/IEEE Conf. on Superconducting (SC '08),
 * Piscataway, NJ: IEEE Press, 2008, pp. Art. 31:1-11. 
 *
 */

// Utilities and system includes
#include <shrUtils.h>
#include "cutil_inline.h"
#include <errno.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE // for pthread_setaffinity_np
#endif
#include <pthread.h>

// includes, kernels
#include "matrixMul_kernel.cu"

//static char *sSDKsample = "matrixMul";

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
void * runTest(void * arg);
void randomInit(float*, int);
void printDiff(float*, float*, int, int, int, float);

extern "C"
void computeGold(float*, const float*, const float*, unsigned int, unsigned int, unsigned int);

#define printd(fmt, args...) \
{ \
	printf("pid-%lu ", pthread_self()); \
	printf(fmt, ##args); \
	fflush(stdout); \
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	int err;
	int numGPUs;

	cudaGetDeviceCount(&numGPUs);
	printf("%d GPUs %d threads\n", numGPUs, numGPUs);

	pthread_t *tids = (pthread_t*)malloc(numGPUs * sizeof(pthread_t*));
	if (!tids)
		return -1;

	int numThreads = numGPUs;
	int threadNum;
	for (threadNum = 0; threadNum < numThreads; threadNum++) {
		int *gpu = (int*)malloc(sizeof(*gpu));
		*gpu = threadNum;
		err = pthread_create(&tids[threadNum], NULL, runTest, (void*)gpu);
		if (err < 0) {
			perror("Spawning");
			return -1;
		}
	}

	// TODO Maybe replace this with a barrier that all threads join on when they
	// exit, instead of waiting on all threads in order.
	printd("joining\n");
	for (threadNum = 0; threadNum < numThreads; threadNum++) {
		err = pthread_join(tids[threadNum], NULL);
		if (err < 0)
			perror("joining");
	}

	printf("cudaThreadExit()\n");
    cudaThreadExit();

	free(tids);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void * runTest(void * arg)
{
    cudaDeviceProp props;
	int gpu = *((int*)arg);
	free(arg);
	cpu_set_t cpuset;

	printf("%lu alive, using gpu %d\n", pthread_self(), gpu);
	cudaSetDevice(gpu);
	CPU_ZERO(&cpuset);
	CPU_SET((gpu * 2), &cpuset); // XXX Assuming 2 * gpuID is a cpu core ID on the same NUMA domain
	pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset);

    // get number of SMs on this GPU
    cutilSafeCall(cudaGetDevice(&gpu));
    cutilSafeCall(cudaGetDeviceProperties(&props, gpu));

	// set seed for rand()
    srand(pthread_self());

    // Optional Command-line multiplier for matrix sizes
    unsigned int uiWA, uiHA, uiWB, uiHB, uiWC, uiHC;
    int iSizeMultiple = 10;

	// optional arg for specifying how many iterations the main kernel will be
	// executed
	int nIter = 1;

	// For GPUs with fewer # of SM's, we limit the maximum size of the matrix
	if (props.multiProcessorCount <= 4) {
		uiWA = 2 * BLOCK_SIZE * iSizeMultiple;
		uiHA = 4 * BLOCK_SIZE * iSizeMultiple;
		uiWB = 2 * BLOCK_SIZE * iSizeMultiple;
		uiHB = 4 * BLOCK_SIZE * iSizeMultiple;
		uiWC = 2 * BLOCK_SIZE * iSizeMultiple;
		uiHC = 4 * BLOCK_SIZE * iSizeMultiple;
	} else {
		uiWA = WA * iSizeMultiple;
		uiHA = HA * iSizeMultiple;
		uiWB = WB * iSizeMultiple;
		uiHB = HB * iSizeMultiple;
		uiWC = WC * iSizeMultiple;
		uiHC = HC * iSizeMultiple;
	}

    // allocate host memory for matrices A and B
    unsigned int size_A = uiWA * uiHA;
    unsigned int mem_size_A = sizeof(float) * size_A;
    float* h_A = (float*)malloc(mem_size_A);
    unsigned int size_B = uiWB * uiHB;
    unsigned int mem_size_B = sizeof(float) * size_B;
    float* h_B = (float*)malloc(mem_size_B);

    // initialize host memory
	printd("initializing host memory\n");
    randomInit(h_A, size_A);
    randomInit(h_B, size_B);

    // allocate device memory
	printd("allocating device memory\n");
    float* d_A;
    cutilSafeCall(cudaMalloc((void**) &d_A, mem_size_A));
    float* d_B;
    cutilSafeCall(cudaMalloc((void**) &d_B, mem_size_B));

    // copy host memory to device
	printd("copying memory to device\n");
    cutilSafeCall(cudaMemcpy(d_A, h_A, mem_size_A,
                              cudaMemcpyHostToDevice) );
    cutilSafeCall(cudaMemcpy(d_B, h_B, mem_size_B,
                              cudaMemcpyHostToDevice) );

    // allocate device memory for result
    unsigned int size_C = uiWC * uiHC;
    unsigned int mem_size_C = sizeof(float) * size_C;
    float* d_C;
    cutilSafeCall(cudaMalloc((void**) &d_C, mem_size_C));

    // allocate host memory for the result
    float* h_C = (float*) malloc(mem_size_C);
    

    // setup execution parameters
    dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid(uiWC / threads.x, uiHC / threads.y);

    // kernel warmup
	printd("warmup kernel\n");
    matrixMul<<< grid, threads >>>(d_C, d_A, d_B, uiWA, uiWB);
    cudaThreadSynchronize();
    
    // create and start timer
    unsigned int timer = 0;
    cutilCheckError(cutCreateTimer(&timer));
    cutilCheckError(cutStartTimer(timer));

    // execute the kernel
	printd("multi-kernel exec\n");
    for (int j = 0; j < nIter; j++) {
            matrixMul<<< grid, threads >>>(d_C, d_A, d_B, uiWA, uiWB);
    }
    cudaThreadSynchronize();

    // stop and destroy timer
    cutilCheckError(cutStopTimer(timer));
    double dSeconds = cutGetTimerValue(timer)/((double)nIter * 1000.0);
    double dNumOps = 2.0 * (double)uiWA * (double)uiHA * (double)uiWB;
    double gflops = 1.0e-9 * dNumOps/dSeconds;

    //Log througput, etc
    printf("mult %d A %ux%u B %ux%u C %ux%u ",
			iSizeMultiple, uiWA, uiHA, uiWB, uiHB, uiWC, uiHC);
    printf("%.4f gflops %.5f sec \n", gflops, dSeconds);
    cutilCheckError(cutDeleteTimer(timer));

    // clean up memory
    free(h_A);
    free(h_B);
    free(h_C);
	printd("freeing device memory\n");
    cutilSafeCall(cudaFree(d_A));
    cutilSafeCall(cudaFree(d_B));
    cutilSafeCall(cudaFree(d_C));

	pthread_exit(0);
}

// Allocates a matrix with random float entries.
void randomInit(float* data, int size)
{
    for (int i = 0; i < size; ++i)
        data[i] = rand() / (float)RAND_MAX;
}
