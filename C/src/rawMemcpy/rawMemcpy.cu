/**
 * rawMemcpy.cu
 *
 * @author Alex Merritt, merritt.alex@gatech.edu
 * @date 2012-01-20
 *
 * Program enabling tweaking of #RPCs and data sizes independently. Data size of
 * zero can be specified to make the calls as light-weight as a nullcall. Zero
 * data size also means the kernel performs no work, but is still invoked.
 */

#include <stdio.h>

/** Simple kernel. Also needed so the interposers connects to the runtime. */
__global__ void kernel(float *array, size_t amt)
{
	//if (amt > 0) array[amt/2] = 0xdeadbeef;
	return;
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("%s MiB iterations\n", argv[0]);
		return -1;
	}
	size_t amt = 1024 * size_t(atoi(argv[1]));
	size_t iters = size_t(atoi(argv[2]));
	bool printNewline = !(argc == 4 && strncmp(argv[3],"-nonewline",11) == 0);
	printf("bytes %lu memcpy-rpcs %lu", amt, (iters << 1));
	if (printNewline)
		printf("\n");
	else
		printf(" ");

	cudaSetDevice(0);

	void *devPtr;
	if (cudaMalloc(&devPtr, amt) != cudaSuccess) {
		printf("Could not allocate on device\n");
		return -1;
	}
	void *hostPtr = malloc(amt);
	if (!hostPtr) {
		printf("Could not allocate on host\n");
		cudaFree(devPtr);
		return -1;
	}

	cudaError_t cudaErr = cudaSuccess;
	int iter;
	for (iter = 0; iter < iters; iter++) {
		cudaErr = cudaMemcpy(devPtr, hostPtr, amt, cudaMemcpyHostToDevice);
		if (cudaErr != cudaSuccess) {
			printf("Error copying: %s\n", cudaGetErrorString(cudaErr));
			break;
		}
		kernel<<<1,1>>>((float*)devPtr, amt/sizeof(float));
		cudaErr = cudaGetLastError();
		if (cudaErr != cudaSuccess) {
			printf("Error launching: %s\n", cudaGetErrorString(cudaErr));
			break;
		}
		cudaThreadSynchronize();
		cudaErr = cudaMemcpy(hostPtr, devPtr, amt, cudaMemcpyDeviceToHost);
		if (cudaErr != cudaSuccess) {
			printf("Error copying: %s\n", cudaGetErrorString(cudaErr));
			break;
		}
	}

	cudaFree(devPtr);
	free(hostPtr);

	return 0;
}
